defmodule Reddish do
  @moduledoc """
  Reddish controls Red instance via stdio
  """

  alias Porcelain.Process, as: Proc
  alias Porcelain.Result

  @doc """
  Starts Red instance
  """
  def start do
    Porcelain.spawn_shell("red", in: :receive, out: {:send, self()})

    # Proc.send_input(proc, "ohai proc\n")
    # receive do
    #   {^pid, :data, :out, data} -> IO.inspect data   #=> "ohai proc\n"
    # end

    # Proc.send_input(proc, "this won't match\n")
    # Proc.send_input(proc, "ohai")
    # Proc.send_input(proc, "\n")
    # receive do
    #   {^pid, :data, :out, data} -> IO.inspect data   #=> "ohai\n"
    # end
    # receive do
    #   {^pid, :result, %Result{status: status}} -> IO.inspect status   #=> 0
    # end
  end

  def send_input(instance, text) do
    Proc.send_input(instance, text)
  end

  def stop(instance) do
    Proc.stop(instance)
  end

  def alive?(instance) do
    Proc.alive?(instance)
  end

end
